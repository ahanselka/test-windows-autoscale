FROM mcr.microsoft.com/windows/servercore:ltsc2022

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

RUN echo $PSVersionTable

RUN Invoke-WebRequest -Method GET -Uri "https://api.ipify.org/"

CMD ["echo", "Hello world!"]

